const AWS = require('aws-sdk');
const igdb = require('igdb-api-node').default;

const searchFields = [
                'name',
                'slug',
                'first_release_date',
                'storyline',
                'summary',
                'genres',
                'genres.id',
                'genres.name',
                'genres.slug',
                'involved_companies',
                'involved_companies.developer',
                'involved_companies.publisher',
                'involved_companies.company',
                'involved_companies.company.name',
                'involved_companies.company.id',
                'involved_companies.company.slug',
                'screenshots',
                'screenshots.id',
                'screenshots.image_id',
                'screenshots.url',
                'screenshots.width',
                'screenshots.height'
];

exports.handler = async (event, context, callback) => {

    console.log(event);
    const client = igdb(process.env.IGDB_CLIENT_ID, process.env.IGDB_ACCESS_TOKEN);
    const SNS = new AWS.SNS();
    if (event.Records) {
        console.log('S3 event');
        for (var recordIndex in event.Records) {
            let record = event.Records[recordIndex]; 
            let zipName = getFileName(record.s3.object.key);
            let searchTerm = getSearchTermFromZip(zipName);
            console.log(zipName);
            let response = await client.fields(searchFields)
                .search(searchTerm)
                .where('first_release_date < 946684800 & platforms = [13]')
                .request('/games');

            let res = await SNS.publish({
                Message: JSON.stringify({
                    data: response.data,
                    zip_name: zipName
                }),
                TopicArn: 'arn:aws:sns:eu-central-1:177646434982:dos4free-test'
            }).promise();

            console.log(JSON.stringify(response.data));
            console.log(res);
        }
    }

    if (event.queryStringParameters) {
        console.log('Web request event');
        let zipName = getFileName(event.queryStringParameters['zip_name']);
        let searchTerm = getSearchTermFromZip(zipName);

        console.log(zipName);
        console.log(searchTerm);

        let response = await client.fields(searchFields)
            .search(searchTerm)
            .where('first_release_date < 946684800 & platforms = [13]')
            .request('/games');

        response.data.zip_name = zipName;
        console.log(response.data);
        callback(null, {
            statusCode: 200,
            body: JSON.stringify({
                data: response.data,
                zip_name: zipName
            }),
            headers: {'Content-Type': 'application/json'}
        });
    } 
};

function getFileName(zipName) {
    let splits = zipName.split('/'); // Splitting by path seperator
    return splits.pop(); // We only need the filename
}

function getSearchTermFromZip(zipName) {
    let splits = zipName.split('/'); // Splitting by path seperator
    let filename = splits.pop(); // We only need the filename
    let parts = filename.split('.'); // Splitting by . since files are name keen.dreams.zip
    parts.pop(); // Removing extension
    return parts.join(' '); // Returning searchterm
    
}
